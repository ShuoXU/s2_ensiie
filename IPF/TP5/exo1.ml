 
  (*1.1*)
let partition p l = List.fold_right (function x -> function (l1, l2) -> if (p x) then (x::l1,l2) else (l1,x::l2)) l ([],[]);;

  
  (*1.2*)
type arbre_binaire =
  | Vide
  | Noeud of int * arbre_binaire * arbre_binaire;;

let rec quicksort_demo l = match l with [] -> []
                                 | e::r -> let (l1,l2) = partition (function x -> x <= e) r in
                                           (quicksort l1)@(e::(quicksort l2));;

  
  (*1.3*)
let comp a b =  if a < b then -1 else if a = b then 0 else 1;;  
  
let rec quicksort comp l = match l with [] -> []
                                 | e::r -> let (l1,l2) = partition (function x -> (comp x e) < 0) r in
                                           (quicksort comp l1)@(e::(quicksort comp l2));;

  (*1.4*)
let couple = [(5,2);(9,8);(1,5);(6,3);(8,8);(5,5);(5,1)];;
quicksort comp couple;;


  (*2.1*)
type forme = Point | Cercle of float | Rectangle of float*float;;
type surface = S of float;;
let aire f ;;
let rec somme_aire l = match l with [] -> S 0.0
                                  | f::r -> add_surface (aire f)(somme_aire r);;
let add_surface s1 s2 = match (s1,s2) with (S f1, S f2) -> S(f1+.f2);;
let somme_aire f = List.fold_right (function x -> function f -> add_surface x (aire f)) (S 0.0) l;;
let exist_aire l = List.exists (function f-> match f with Rectangle(lo,la) -> lo=la | _ -> false) l;;
  
