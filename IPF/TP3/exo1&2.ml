  (*test*)
let rec fact n = if n=0 then 1 else n * fact(n-1);;

  (*1.1*)
let rec repeat n x = if n = 0 then [] else x::(repeat (n-1) x);;

  (*1.2*)
let rec nb_occ x l = match l with
  | [] -> 0
  | a::r -> if a=x then 1 + (nb_occ x r) else (nb_occ x r);;

  (*1.3*)
let rec last l = match l with
  | [] -> failwith "La liste est vide!!"
  | a::r -> if r = [] then a else last r;;
  
  (*1.4*)
let rec is_increasing l = match l with
  | [] -> true
  | x1::[] -> true
  | x1::x2::r -> if x1 < x2 then (is_increasing (x2::r)) else false;;

(*1.5*)
let rec somme l = match l with
  | [] -> 0
  | a::r -> a + (somme r);;
let rec nb l = match l with
  | [] -> 0
  | _::r -> 1 + (nb r);;
let rec moyenne l = if l=[] then failwith "La liste est vide."
                    else (float_of_int(somme l)) /. (float_of_int(nb l));;
let rec somme_nb l = match l with
  | [] -> (0,0)
  | x::r -> let (a,b) = (somme_nb r) in (x+a, 1+b);;
let rec moyenne1 l = if l=[] then failwith "La liste est vide."
                     else let (a,b) = (somme_nb l) in (float_of_int a) /. (float_of_int b);;


  (*2.1*)
let rec insert x l = match l with
  | [] -> [x]
  | a::r -> if a > x then x::a::r else a::(insert x r);;

  (*2.2*)
let rec tri_insertion l = match l with
  | [] -> []
  | a1::r -> let e = (tri_insertion r) in (insert a1 e);; 

  (*2.3*)  
let rec fusion l1 l2 = match (l1,l2) with
  | ([],_) -> l2 
  | (_,[]) -> l1
  | (x1::r1, x2::r2) -> if x1 <= x2 then x1::(fusion r1 l2)
                        else x2::(fusion r2 l1);;
