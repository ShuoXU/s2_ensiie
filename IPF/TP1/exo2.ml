(*2.1*)
let rec terme n =
  if n = 0 then 1.0 else sqrt(terme (n-1) +. 2.0);;

(*2.2*)
let rec somme n =
  if n = 0 then 0 else n + somme(n-1);;

(*2.3*)
let rec carre n =
  if n = 0 then 0 else n*n + carre(n-1);;

(*2.4*)
let dev x = x mod 2;;
let carrer x = x*x;;
let rec puissance a n =
  if n = 1 then a else
    if dev n = 0 then puissance (carrer a) (n/2)
    else a * puissance (carrer a) ((n-1)/2);;

(*2.5*)
let rec putain a n =
  if n = 1 then a else
    if dev n = 0 then carrer (puissance a (n/2))
    else a * carrer (puissance a ((n-1)/2));;
  
