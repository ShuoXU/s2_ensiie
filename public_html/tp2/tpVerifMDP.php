<?php

include("tpVue.php");
include("tpModele.php");


//recuperer la valeur saisie dans le champ "mdp"
$mdp = $_POST['mdp'];

enTete("Vérification du mot de passe");


if(@verif_mdp($mdp)){
  $_SESSION['nomuser'] = 'tpphp';
  header('Location: index.php');
}
else {
  affiche_erreur("mot de passe vide ou faux. Merci de réessayer");
  echo "<a href= 'tpConnexion.php'>Se connecter</a>";
}

/*
  Si le mot de passe entré est vide ou faux (cf. fonction verif_mdp), afficher une erreur et un lien vers tpConnexion.php

  S'il est bon, rediriger vers la page index.php
*/


pied();

?>
