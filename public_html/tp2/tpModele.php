<?php
session_start();
include("config.php");
include("db.php");

function verif_authent(){
    global $AUTHENT;
    if ($AUTHENT == 1 && !array_key_exists( 'nomuser', $_SESSION)){
      header('Location: tpConnexion.php');
    }
    /*
      Complétez cette fonction pour que si la variable AUTHENT est positionnée à 1 et que la variable de session
      comprenant le nom d'utilisateur nomuser n'est pas positionnée, l'utilisateur soit redirigé vers la page tpConnexion.php
      (voir fonction header() de php)
    */
}

function config() {
    global $nom_hote, $nom_user, $nom_base, $mdp;
    $_SESSION['nomhote'] = $nom_hote;
    $_SESSION['nombase'] = $nom_base;
    $_SESSION['nomuser'] = $nom_user;
    $_SESSION['mdp'] = $mdp;
}



function get_client($numCli)
{
	if ($db = db_connect()){
		$req = "SELECT * FROM client WHERE num_client = '$numCli'";
		$rep = db_query($db, $req);
		db_close($db);
		if ($rep){
			$nb_row = db_count($rep);
			if ($nb_row == 0){
				return array(-1,0,0);
			}
			else{
				$info_client = db_fetch($rep);
				return array(0, $info_client['nom_client'], $info_client['debit_client']);
			}
		}
		else{
			return array(-2,0,0);
		}
		db_close($db);
	}
	else {
		return array(-3, 0, 0);
	}

/*
Cette fonction doit exécuter une requête SELECT permettant de récupérer le nom et le débit d'un client de numéro donné
et retourner un tableau comprenant trois valeurs:
- une première valeur donnant une information sur l'exécution: 0 si tout s'est bien passé, -1 si aucun client n'a été trouvé avec le numéro donné, -2 si la requête n'a pas pu être exécutée, -3 si la connexion à la BD n'a pas réussi
- la seconde valeur donne le nom du client si tout s'est bien passé, 0 sinon
- la troisième valeur donne le débit du client si tout s'est bien passé, 0 sinon
*/

}

function insert_achat($numCli,$montant) {
  if ($db = db_connect()){
    $reqdebit = "SELECT debit_client FROM client WHERE num_client = '$numCli'";
    $repdebit = db_query($db, $reqdebit);
    if ($repdebit){
      $debitInit = db_fetch($repdebit)['debit_client'];
      $debitMod = $debitInit - $montant;
      $date = date('Y/m/d');
      $reqcli = 'UPDATE client SET debit_client = \''.format_number(test_input($debitMod)).'\' WHERE num_client = '.test_input($numCli);
      $reqachat = "INSERT INTO achat(montant_achat,date_achat,client) VALUES (".format_number(test_input($montant)).",'".test_input($date)."','".test_input($numCli)."')";
      $query_array = [$reqcli,$reqachat];
      db_transaction($db,$query_array);
      db_close($db);
      return true;
    }
    else {
      db_close($db);
      echo "erreur de modification de la base de donnée";
      return false;}

  }
  else {
    echo "erreur de connexion à la base de donnée";
    db_close($db);
    return false;
  }
/*
  Cette fonction doit effectuer une transaction qui:
  - met à jour le débit du client du numéro concerné (après avoir testé la valeur entrée par l'utilisateur si possible)
  - insère un nouvel achat dans la base
  Il faudra utiliser une transaction pour garder une base cohérente même en cas d'erreur.
*/
}


function create_client($numCli,$nomCli,$debitInit) {

    if ( $db = db_connect()) {
		$req = "INSERT INTO client(num_client,nom_client,debit_client) VALUES (".test_input($numCli).",'".test_input($nomCli)."','".format_number(test_input($debitInit))."')";
		db_query($db,$req);
		db_close( $db );
		return true;
	}
	else {
		return false;
	}
}


function set_client($numCli,$nomMod,$debitMod) {
    if ( $db = db_connect()) {
        $req = 'UPDATE client SET nom_client = \''.test_input($nomMod).'\', debit_client = \''.format_number(test_input($debitMod)).'\' WHERE num_client = '.test_input($numCli);

        $rep = db_query($db,$req);
        db_close( $db );

        return true;
    }
    else {
        return false;
    }

}

function verif_mdp($mdp) {

	if ($db = db_connect()) {
		$_SESSION['mdp']=$mdp;
		db_close($db);
		return true;
	} else
		return false;

}

function detruire_session() {
	session_destroy();
}




function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}





function format_number($str) {
  return str_replace(',','.',$str);
}
