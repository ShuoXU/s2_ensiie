<?php

include("tpModele.php");
include("tpVue.php");

verif_authent();

enTete("Gestion des clients");


/* recupération du numéro de client entré dans le formulaire de menu */
$numCli = $_POST['numCli'];

// si aucune case n'a été cochée
if(! isset($_POST['choix']) ){
    affiche_erreur("Merci de cocher une case");
}
// si le numéro de client n'a pas été renseigné
elseif ((! isset($numCli)) | $numCli == "" | ! is_numeric($numCli)) {
    affiche_erreur("Vous devez impérativement taper un numéro de client");
}
else{
    switch ($_POST['choix'])
        {
        /* Affichage d'un client */
        case "v":
	    affiche_info("Consultation du numéro client : $numCli");
	    $tab=get_client($numCli);
        $etat = $tab[0];
        switch ($etat)
        {
        case 0:
            echo "Nom: $tab[1], Débit: $tab[2]";
            break;
        case -1:
            echo "Aucun client ne correspond au numéro donné";
            break;
        case -2:
            echo "Erreur requête";
            break;
        case -3:
            echo "Erreur connexion";
            break;
        }
        break;

        /* Modification d'un client */
        case "m" :
	    affiche_info("Modification du client no : $numCli");
      $tab=get_client($numCli);
        $etat = $tab[0];
        switch ($etat)
        {
        case 0:
            affiche_form_modif($numCli, $tab[1], $tab[2]);
            break;
        case -1:
            echo "Aucun client ne correspond au numéro donné";
            break;
        case -2:
            echo "Erreur requête";
            break;
        case -3:
            echo "Erreur connexion";
            break;
        }

	    /* Compléter pour faire appel à la fonction affiche_form_modif() */
        break;

        /* Enregistrement d'un achat */
        case "a" :
	    affiche_info("Achat du client no : $numCli");
      $tab=get_client($numCli);
        $etat = $tab[0];
        switch ($etat)
        {
        case 0:
            affiche_form_achat($numCli);
            break;
        case -1:
            echo "Aucun client ne correspond au numéro donné";
            break;
        case -2:
            echo "Erreur requête";
            break;
        case -3:
            echo "Erreur connexion";
            break;
        }
	    /*Compléter : récupérer les infos sur le client et appeler la fonction affiche_form_achat() */
        break;

        /* Création d'un client */
        case "c" :
            affiche_info("Création du numéro client : $numCli");
            $tab=get_client($numCli);
              $etat = $tab[0];
              switch ($etat)
              {
              case 0:
                  echo "Le numéro entré ('$numCli') est déjà pris";
                  break;
              case -1:
                  affiche_form_creation($numCli);
                  break;
              case -2:
                  echo "Erreur requête";
                  break;
              case -3:
                  echo "Erreur connexion";
                  break;
              }
	    /*Compléter : vérifier si le client existe, et appeler la fonction affiche_form_creation() si ce n'est pas le cas */
        break;

        /* ne devrait jamais se produire... */
        default :
            affiche_erreur("Cocher une case");
        break;
    }//fin switch

}// fin else

retour_menu();
pied();

?>
