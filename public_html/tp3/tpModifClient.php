<?php

include("tpModele.php");
include("tpVue.php");

verif_authent();

$numCli=$_POST['numCli'];
$nomMod=$_POST['nomMod'];
$debitMod=$_POST['debitMod'];

enTete("Modification d'un client");

/*
 Faire appel à la fonction set_client, et afficher un message indiquant que la mise à jour a été effectuée, ou un message d'erreur
*/
if (set_client($numCli, $nomMod, $debitMod)){
    affiche_info("Mise à jour éffectuée!");
}
else{
    affiche_info("Mise à jour échoué!");
}



retour_menu();
pied();
?>


