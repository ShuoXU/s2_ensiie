<?php

include("tpModele.php");
include("tpVue.php");

verif_authent();

enTete("Gestion des Employés");


/* recupération du numéro de client entré dans le formulaire de menu */
$numCli = $_POST['numCli'];

// si aucune case n'a été cochée
if(! isset($_POST['choix']) ){
    affiche_erreur("Merci de cocher une case");
}
// si le numéro de client n'a pas été renseigné
elseif ((! isset($numCli)) | $numCli == "" | ! is_numeric($numCli)) {
    affiche_erreur("Vous devez impérativement taper un numéro de client");
}
else{	
    switch ($_POST['choix'])
    {
        /* Affichage d'un client */
        case "v":
	    affiche_info("Consultation du numéro employé : $numCli");
	/*
	  Compléter pour afficher les informations demandées ou le cas d'erreur approprié
	*/
	    $tab = get_client($numCli);
	    if (resultat_get_client($tab[0]) == true) {
            affiche_info("Nom : $tab[6], Service : $tab[2], Budget : $tab[4]€, Salaire : $tab[5], Chef : $tab[3]");
        }
        break;

        /* Modification d'un client */
        case "m" :
	        affiche_info("Modification du client no : $numCli");
	    /* Compléter pour faire appel à la fonction affiche_form_modif() */
            $tab=get_client($numCli);
            if (resultat_get_client($tab[0]) == true) {
                affiche_form_modif($numCli, $tab[1],$tab[2]);
        }
            break;
            
        /* Enregistrement d'un achat */
        case "a" :
	    affiche_info("Achat du client no : $numCli");
	    /*Compléter : récupérer les infos sur le client et appeler la fonction affiche_form_achat() */
            $tab=get_client($numCli);
            if (resultat_get_client($tab[0]) == true) {
                affiche_form_achat($numCli, $tab[2]);
            }
            break;

        /* Création d'un client */
        case "c" :
            affiche_info("Création du numéro client : $numCli");
	    /*Compléter : vérifier si le client existe, et appeler la fonction affiche_form_creation() si ce n'est pas le cas */
            $tab=get_client($numCli);
            if (resultat_get_client($tab[0]) == true) {
                affiche_erreur("Cette numéro de client déjà existe.");
                //affiche_form_creation($numCli);
            }
            else{
                
                affiche_form_creation($numCli);
                echo '<br/><a href="index.php">Menu</a><br/>';
            }
            break;
            
        break;

        /* ne devrait jamais se produire... */
        default : 
            affiche_erreur("Cocher une case");
        break;
    }//fin switch
	
}// fin else
	
retour_menu();
pied();

?>



