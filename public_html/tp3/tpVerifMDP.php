<?php

include("tpVue.php");
include("tpModele.php");
include("index.php");
include("tpConnexion.php");


//recuperer la valeur saisie dans le champ "mdp" 
$mdp = $_POST['mdp']; 

enTete("Vérification du mot de passe");

/*
  Si le mot de passe entré est vide ou faux (cf. fonction verif_mdp), afficher une erreur et un lien vers tpConnexion.php
 
  S'il est bon, rediriger vers la page index.php
*/
if($mdp == NULL){
    affiche_erreur("MDP vide!!!");
}

if( verif_mdp($mdp)) {
    echo '<br/><a href="index.php">Menu</a><br/>';
}
else {
	affiche_erreur("La faute MDP!!!");
    echo '<br/><a href="tpConnexion.php">Reconnexion</a>';
}


pied();

?>

