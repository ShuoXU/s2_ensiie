#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>

void copy(PLAN grand, PLAN tmp);

int compte(PLAN grand, char c);

int point_F(PLAN grand);

int point_R(PLAN grand);

void FloodFill(points p,  char c, PLAN tmp);

int point_V(PLAN grand);

int point_L(PLAN grand);

int points_total(PLAN grand);

int point_F2(PLAN grand);

int point_R2(PLAN grand);

int point_P2(PLAN grand);

int point_V2(PLAN grand);

int point_L2(PLAN grand);

int factoriel(int x);

int points_total(PLAN grand);

int points_total2(PLAN grand);

bool carre(PLAN tmp, int x, int y, char c, int buf);

int village(PLAN tmp, int x, int y);
