#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include "struct.h"
#include "alloc.h"
#include "draw.h"
#include "stack.h"
#include "affichage.h"
#include "rotation.h"
#include "insertion.h"
#include "points.h"
#define MAX_SIZE 100



void  stack_init(pile *ps)
{
    ps->count = 0;
}


int est_vide(pile* ps)
{
    if (ps->count == 0) return 1;
    return 0;
}

void push(pile* ps, couple tmp)
{    
  if ( ps->count == MAX_SIZE ) printf("Le pile est trop rempli.");
    ps->elemCouple[ps->count] = tmp;
    ps->count++;
}

couple pop(pile* ps)
{
  couple tmp;
    if(est_vide(ps))
    {
	return tmp;
    }
    else{
      ps->count--;
      tmp = ps->elemCouple[ps->count];
      return tmp;
    }
}

void array_print(couple arr[], int size)
{
    int i;
    for (i = 0; i < size; i++)
    {
      printf("[Type:%c, Id:%d]  ", arr[i].elemChar, arr[i].elemInt);
    }
    printf("\n");
}


void print_stack(pile *ps)
{
     printf("MA PILE: \n");
     if (est_vide(ps))
        printf("EST VIDE");
     array_print(ps->elemCouple, ps->count);
}

void affiche(matrix ret)
{
    int i , j, t ;
    printf(" \t");
    for(t = 0; t < ret.width; t++)
      printf("%d\t",t);
    printf("\n");
    for (i = 0; i < ret.length; i++)
    {
      printf("%d\t",i);
      for (j = 0; j < ret.width; j++)
        {
	  if(ret.cells[i][j] == 'E')
            printf("%c\t",'_');
	  else
	    printf("%c\t", ret.cells[i][j]);
        }
      printf("\n\n");
    }
}

void affiche_of_int(matrix_of_int ret)
{
    int i , j;
    for (i = 0; i < ret.length; i++)
    {
      for (j = 0; j < ret.width; j++)
        {
	  printf("%d\t",  ret.cells[i][j]);
        }
      printf("\n");
    }

    printf("\n");
}

/*
L'allocation d'une matrice de n*m;

*/
matrix allocation (int n, int m){
  matrix test ;
  int a;
  test.cells = (char **)malloc(n * sizeof(char *));
  test.length = n;
  test.width = m;
  for(a = 0; a < n; a++)
    test.cells[a] = (char *)malloc(m * sizeof(char));

  int i,j ;
  for (i = 0; i < n ; i++)
    for ( j = 0 ; j < m ; j++)
      test.cells[i][j] = 'E';
  
  return test;
}

matrix_of_int allocation_of_int (int n, int m){
  matrix_of_int test ;
  int a,i,j;
  test.cells = (int **)malloc(n * sizeof(int *));
  test.length = n;
  test.width = m;
  for(a = 0; a < n; a++){
    test.cells[a] = (int *)malloc(m * sizeof(int));
  }

  for (i = 0; i < n ; i++)
    for (j = 0 ; j < m ; j++)
      test.cells[i][j] = -1;
  
  return test;
}
/*
void free_matrice(matrix x){
  int i;
  for (i = 0; i < x.length; i++)
    free(x.cells[i]);
  free(x.cells);
}

void free_matrice_of_int(matrix_of_int x){
  int i;
  for ( i = 0; i < x.length; i++){
    free(x.cells[i]);
  }
  free(x.cells);
}
*/





tuile creer_tuile_aleatoire (int ID) {
  char terrain[6] = {'V','U','F','R','P','L'};
  tuile test;
  int i,j;
  test.casse = allocation(3,2);
  for (i = 0; i < 3; i++){
    for (j = 0; j < 2; j++){
      test.casse.cells[i][j] = terrain[rand()%6];  
    }
  }
  
  test.coordonnee[0] = 0;
  test.coordonnee[1] = 0;
  test.id = ID;
  affiche(test.casse);
  return test;
}

tuile creer_tuile_fixe (int n){
  tuile test;
  test.casse = allocation(3,2);
  FILE *fichier = fopen("ressources/data.txt", "r;");
  char buf[4];
  int tmp = 0;
  do{
    fgets(buf, 5, fichier);
    tmp = atoi(buf);
    if(tmp == n)
      break;
    else {
      int a;
      for(a = 0; a < 3; a++)
	 fgets(buf, 5, fichier);
	}
  }while(1);
  int i;
  for (i = 0; i < 3 ; i++){
    fgets(buf, 5, fichier);
    test.casse.cells[i][0] = buf[0];
    test.casse.cells[i][1] = buf[2];
  }
  fclose(fichier);
  test.coordonnee[0] = 0;
  test.coordonnee[1] = 0;
  return test;
}

tuile tirer_carte_fixe (int stack[], int n, int ID){
  stack[n] = 1;
  tuile tmp = creer_tuile_fixe(n);
  tmp.id = ID;
  return tmp;
}


tuile tirer_carte_aleatoire (int stack[], int ID){
  int n;
  srand(time(NULL));
  do{
    n = rand()%21;
    if (stack[n] == 0)
      break;
  }while(1);
  
  stack[n] = 1;
  tuile tmp = creer_tuile_fixe(n);
  tmp.id = ID;

  return tmp;
}

/*la direction de inverse du sens de la montre*/
tuile rotation (tuile m) {
  tuile ret;
  int j,k;
  ret.casse.length = m.casse.width;
  ret.casse.width = m.casse.length;
  ret.casse = allocation(m.casse.width, m.casse.length);
  ret.id = m.id;
  for(j = 0; j < m.casse.length; j++)  
    {  
      for(k = 0 ; k < m.casse.width ; k++)  
	{  
	  ret.casse.cells[m.casse.width-1-k][j] = m.casse.cells[j][k];
	}  
    }
  ret.coordonnee[0] = m.coordonnee[0];
  ret.coordonnee[1] = m.coordonnee[1];
  return ret;
}

tuile rotation_n (tuile  m, int n){
  if (n == 0){
    return m;
  }
  
  else {  
    int i;
    for(i = 0; i < n; i++){
      m = rotation(m);
    }
    return m;
  }
}








bool dehors_borne (tuile petit, PLAN grand,int x, int y){
  x++;
  y++;
  if(petit.casse.length == 3){
    if(x > grand.plan.length-2 || y > grand.plan.width-1){
      printf("La tuile ne peut pas etre insere en dehors du plateau. Veuillez choisir une autre coordonnee.\n");
      return true;
    }
  }
  
  if(petit.casse.width == 3){
    if(x > grand.plan.length-1 || y > grand.plan.width-2){
      printf("La tuile ne peut pas etre insere en dehors du plateau. Veuillez choisir une autre coordonnee.\n");
      return true;
    }
  }

  return false;
}

bool lac_decouvert (tuile petit, PLAN grand, int x, int y){

  int i,j;
  for ( i = 0; i < petit.casse.length; i++){
    for ( j = 0; j < petit.casse.width; j++){
      if(grand.plan.cells[x+i][y+j] == 'L'){
	printf("Le lac ne peut pas etre recouvert. Veuillez choisir une autre coordonnee.\n");
	return true;
      }
    }
  }
  return false;
}

void cheki(tuile petit, PLAN grand, int x, int y, pile * tmp_case, int car_usee[]){
  int i, j, a,b;
  for (i = petit.casse.length ; i > 0; i--){
    for (j = petit.casse.width ; j > 0; j--){
      couple buf = pop(tmp_case);
      a = x+i-1, b = y+j-1; 
      grand.plan.cells[a][b] = buf.elemChar;
      grand.flag.cells[a][b] = buf.elemInt;
    }    
  }
  petit.coordonnee[0] = 0;
  petit.coordonnee[1] = 0;
  car_usee[petit.id]  = 0;
  printf("Vous ne pouvez pas recouvrir totalement une carte.\n");
}


bool carte_total_decouvert (PLAN grand, int car_usee[]){
  int i, j, s, k;
  int tmp[] = {0,0,0,0,0,0,0,0,0,0,0,0};
  for (s = 0; s < 12; s++){
    if (car_usee[s] == 1){
      for(i = 0; i < grand.flag.length ; i++){
	for (j = 0 ; j < grand.flag.width; j++){
	  if(grand.flag.cells[i][j] == s){
	    tmp[s] = 1;
	  }
	}
      }
    }
  }
  
  for(k = 0; k < 12; k++){
    if (tmp[k] != car_usee[k]){
      return true;
    }
  }
  return false;
}



void insertion (tuile petit, PLAN grand, int x, int y, pile* tmp_case, int car_usee[]){
  int a, b;
  couple tmp;
  for (a = 0; a < petit.casse.length; a++){
    for (b = 0; b < petit.casse.width; b++){
      tmp.elemChar = grand.plan.cells[x+a][y+b];
      tmp.elemInt  = grand.flag.cells[x+a][y+b];
      push(tmp_case, tmp);
      grand.plan.cells[x+a][y+b] = petit.casse.cells[a][b];
      grand.flag.cells[x+a][y+b] = petit.id;
    }
  }
  car_usee[petit.id] = 1;
  // printf("L'insertion est valide.\n");
}

bool insertion_validation(tuile petit, PLAN grand, int x, int y, int fois, pile* tmp_case,  int car_usee[]){
  printf("\n");
  bool tmp = false;
  if(dehors_borne(petit, grand, x, y))
    goto here;
  if(lac_decouvert(petit, grand, x, y))
    goto here;
  if (fois == 0){
    x = (int)((grand.plan.length-1)/2);
    y = (int)((grand.plan.width-1)/2);
    insertion(petit,grand,x,y,tmp_case,car_usee);
    tmp = true;
    x++;y++;
  }
  
  else{  /*if (fois > 0 && fois < 12){*/
    int i,j;
    int flag = 0;
    for (i = 0; i < petit.casse.length; i++){
      for (j = 0; j < petit.casse.width; j++){
	if(grand.plan.cells[x+i][y+j] != 'E')
	  flag++;
      }
    }
    if (flag == 0){
      printf("Il faut decouvrir au moins une case pas vide.\n");
    }
    else { 
      insertion(petit, grand, x, y, tmp_case,car_usee);
      petit.coordonnee[0] = x;
      petit.coordonnee[1] = y;
      tmp = true;
      if(carte_total_decouvert(grand, car_usee)){
	cheki(petit, grand,x,y,tmp_case,car_usee);
	tmp = false;
      };
      
    }
  }/*
  else {
    printf("La %d ieme fois n'est pas valide.\n", fois);
  }*/
 here:;
  return tmp;
}





void affiche_lot_carte (tuile stack[]){
  int i;
  for (i = 0; i < 12; i++){
    printf("La %dieme carte est:\n ", stack[i].id);
    affiche(stack[i].casse);
  }
}

/*Version de 24/04/2018*/
bool plan_tout_rempli(PLAN grand){
  int i, j;
  for (i = 0; i < grand.plan.length; i++){
    for (j = 0; j < grand.plan.width; j++){
      if (grand.plan.cells[i][j] == 'E'){
	      printf(" Veuillez continuer inserer votre carte.\n");
        return true;
      }
    }
  }
  printf("Le plan est tout rempli.\n");
  return false;
}

void affiche_orizontale (tuile T[], int car_usee[]){
  int i;
  int a,b,c;
  for (i = 0; i < 12; i++){
    if (car_usee[i] == 0){
      printf("id=%d \t", T[i].id);
    }
  }

  printf("\n");
  for (a= 0; a < 12; a++){
    if(car_usee[a]==0){
      printf("%c %c\t", T[a].casse.cells[0][0], T[a].casse.cells[0][1]);
    }
  }
  printf("\n");
  for (b= 0; b < 12; b++){
    if(car_usee[b]==0){
      printf("%c %c\t", T[b].casse.cells[1][0], T[b].casse.cells[1][1]);
    }
  }
  printf("\n");
  for (c= 0; c < 12; c++){
    if(car_usee[c]==0){
      printf("%c %c\t", T[c].casse.cells[2][0], T[c].casse.cells[2][1]);
    }
  }
  printf("\n");
}

int compte(PLAN grand,char c){
  int i, j, cal = 0;
  for(i = 0; i < 10; i++){
    for(j = 0; j < 10; j++){
      if(grand.plan.cells[i][j] == c)
	cal++;
    }
  }
  return cal;
}

int point_F(PLAN grand){
  int tmp = compte(grand, 'F');
  return tmp*2;
}
int point_R(PLAN grand){
  int tmpR = compte(grand, 'R');
  int tmpU = compte(grand, 'U');
  if (tmpU >= tmpR)
    return tmpR*4;
  else
    return tmpU*4;
}

void FloodFill(points point, char c, PLAN tmp){
  int d1[4] = {0, 0, 1, -1};
  int d2[4] = {1, -1, 0, 0};
  int x, y, i;
  tmp.flag.cells[point.x][point.y] = 1; 
  for(i = 0; i < 4; i++){
    x = point.x + d1[i];
    y = point.y + d2[i];
    if(!((x < 0)||(y < 0)||(x > 9)||(y > 9))){
      if((tmp.plan.cells[x][y] == c) && (tmp.flag.cells[x][y] == 0))
	{
	  points buf;
	  buf.x = x;
	  buf.y = y;
	  buf.p = 0;
	  buf.r = 0;
	  tmp.plan.cells[x][y] = 'D';
	  FloodFill(buf, c, tmp);
	}
    }
  }
}

void copy(PLAN grand, PLAN tmp){
  int i, j;
  for (i = 0; i < 10; i++){
    for (j = 0; j < 10; j++){
      tmp.plan.cells[i][j] = grand.plan.cells[i][j];
      tmp.flag.cells[i][j] = 0;
    }
  }
}



int point_V(PLAN grand){
  int i, j, t;
  char c = 'V';
  points point[100]; 

  PLAN tmp;
  tmp.plan = allocation(10, 10);
  tmp.flag = allocation_of_int(10, 10);;
  
  for(i = 0; i < grand.plan.length; i++){
    for(j = 0; j < grand.plan.width; j++){
      point[i*10+j].x = i;
      point[i*10+j].y = j;
      point[i*10+j].r = 0;
      point[i*10+j].p = 0;
      copy(grand, tmp);
      //affiche(tmp.plan);
      if(grand.plan.cells[i][j] == 'V'){
	//printf("Here is [%d %d]\n", i,j);
	FloodFill(point[i*10+j], c, tmp);
	int buf = compte(tmp, 'D') + 1;
	//affiche(tmp.plan);
	//	printf("Buf here is :%d\n",buf);
	point[i*10+j].p = buf;
      }
    }
  }
  int cal = 0;
  for (t = 0 ; t < 100; t++){
    if (cal < point[t].p)
      cal = point[t].p;
  }
  return cal;
}


int point_L(PLAN grand){
  int tmp = compte(grand, 'L');
  return tmp > 0 ? (tmp-1)*3 : 0;
}

int factoriel(int x){
  if (x <= 0)
    return 0;
  else{
    int i, buf = 0;
    for (i = 1; i <= x; i++)
      buf = buf + i;
    return buf;
  }
}


int point_F2(PLAN grand){
  int tmp = compte(grand, 'F');
  if(tmp > 5)
    return factoriel(5);
  else if (tmp == 0)
    return 0;
  else
    return factoriel(tmp);
};

int point_R2(PLAN grand){
  int tmpR = compte(grand, 'R');
  int tmpU = compte(grand, 'U');
  if (2*tmpU >= tmpR)
    return tmpR*4;
  else
    return tmpU*8;
};

int point_P2(PLAN grand){
  int i, j, t;
  char c = 'P';
  points point[100];
  int flag[100];
  PLAN tmp;
  tmp.plan = allocation(10, 10);
  tmp.flag = allocation_of_int(10, 10);;
  
  for(i = 0; i < grand.plan.length; i++){
    for(j = 0; j < grand.plan.width; j++){
      flag[i*10+j] = 0;
      point[i*10+j].x = i;
      point[i*10+j].y = j;
      point[i*10+j].r = 0;
      point[i*10+j].p = 0;
      copy(grand, tmp);
      if(grand.plan.cells[i][j] == 'P'){
	FloodFill(point[i*10+j], c, tmp);
	int buf = compte(tmp, 'D') + 1;
	point[i*10+j].p = buf;
      }
    }
  }
  int cal = 0;
  for (t = 0 ; t < 100; t++){
    if (point[t].p >= 4){
      flag[(point[t].p)]++;
    }
  }
  for (t = 0; t < 100; t++){
    //if ( flag[t] != 0)printf("Avant %d ->",flag[t]);
    flag[t] = flag[t]/4;
    cal = cal + 4 * flag[t];
    //if (flag[t]!=0)printf("Apres %d\n", flag[t]);
  }
  return cal;
};

bool carre(PLAN tmp, int x, int y, char c, int buf){
	bool flag1 = (tmp.plan.cells[x][y] == 'c')&&
    (tmp.plan.cells[x + 1][y + 1] == 'c')&&
    (tmp.plan.cells[x + 1][y] == 'c')&&
    (tmp.plan.cells[x][y + 1] == 'c');
    bool flag2 = (tmp.flag.cells[x][y] == 1)&&
    (tmp.flag.cells[x + 1][y + 1] == 1)&&
    (tmp.flag.cells[x + 1][y] == 1)&&
    (tmp.flag.cells[x][y + 1] == 1);
    if (flag1 && flag2){
    	tmp.flag.cells[x][y] = buf + 1;
    	tmp.flag.cells[x+1][y+1] = buf + 1;
    	tmp.flag.cells[x+1][y] = buf + 1;
    	tmp.flag.cells[x][y+1] = buf + 1;
    }
    return flag1&&flag2;
}

int village(PLAN tmp, int x, int y){
	int i, j ;
	for(i = 0; i < tmp.plan.length; i++){
		for(j = 0; j < tmp.plan.width; j++){
			if(tmp.plan.cells[i][j] == 'D')
				tmp.flag.cells[i][j] = 1;
			else
				tmp.flag.cells[i][j] = 0;
		}
	}

	int cal = 0;
	int buf = 1;
	for(i = 0; i < tmp.plan.length; i++){
		for(j = 0 ; j < tmp.plan.width; j++){
			if(carre(tmp, i, j, 'D', buf)){
				buf++;
				cal = cal *4;	
			}
		}
	}

	affiche_of_int(tmp.flag);
	return cal;
}

int point_V2(PLAN grand){
  int i, j, t;
  char c = 'V';
  points point[100]; 

  PLAN tmp;
  tmp.plan = allocation(10, 10);
  tmp.flag = allocation_of_int(10, 10);;
  
  for(i = 0; i < grand.plan.length; i++){
    for(j = 0; j < grand.plan.width; j++){
      point[i*10+j].x = i;
      point[i*10+j].y = j;
      point[i*10+j].r = 0;
      point[i*10+j].p = 0;
      copy(grand, tmp);
      if(grand.plan.cells[i][j] == 'V'){
		FloodFill(point[i*10+j], c, tmp);
		int buf = compte(tmp, 'D') + 1;
		if (buf > 0)
			tmp.plan.cells[i][j] = 'D';
		int buf2 = buf/4;
		/*printf("here buf2 = %d\n", buf2);*/
		point[i*10+j].p = buf + buf2*4;
      }
    }
  }
  int cal = 0;
  for (t = 0 ; t < 100; t++){
    if (cal < point[t].p)
      cal = point[t].p;
  }
  return cal;
};

int point_L2(PLAN grand){
  int tmp = compte(grand, 'L');
  return tmp > 0 ? (tmp-1)*2 : 0; 
};


int exists(char *nomFichier)
{
    FILE *file;
    if ((file = fopen(nomFichier, "r")))
    {
        fclose(file);
        return 1;
    }
    return 0;
}

void extract(tab v, char *nomFichier)
{
    FILE* fichier = NULL;
    int i=0;    
    fichier = fopen(nomFichier, "r");
    if (fichier != NULL)
    {
      for (i=0; i<15; i++)
	{        fscanf(fichier, "%d", &v[i]);
	}
      fclose(fichier);
    }
}

points sol_bete (tuile petit, PLAN grand, int fois, pile* tmp_case, int car_usee[], int flag2){
  int i = 0, j = 0, f = 0;
  points tmp[400];
  points buf;
  buf.p = 0;
  buf.x = 4;
  buf.y = 4;
  buf.r = 0;
  bool flag;
  if (fois == 0)
    return buf;
  for(f = 0; f < 4; f++){
    if(f!=0)
      petit = rotation_n(petit, 1);
    for(i = 0; i < 10; i++){
      for(j = 0; j < 10; j++){
	tmp[f*100+i*10+j].x = i;
	tmp[f*100+i*10+j].y = j;
	tmp[f*100+i*10+j].p = 0;
	tmp[f*100+i*10+j].r = f;
	flag = insertion_validation(petit, grand, i, j, fois, tmp_case, car_usee);
	if (flag){
		if(flag2 == 1) {
			tmp[f*100+i*10+j].p = points_total(grand);
		}
	  	else{
	  		tmp[f*100+i*10+j].p = points_total2(grand);
	  	}
        printf("[%d %d] %d ->%d\n", i, j, f, tmp[f*100+i*10+j].p);
	  	cheki(petit, grand, i, j, tmp_case, car_usee);
	}
	printf("La %dieme fois. \n", (f*100+i*10+j));
      }
    }
  }
  i = 0;
  j = 0;
  f = 0;
  for(f = 0; f < 4; f++){
    for(i = 0; i < 10; i++){
      for(j = 0; j < 10; j++){
	if(tmp[f*100+i*10+j].p >= buf.p){
	  buf.x = i;
	  buf.y = j;
	  buf.r = f;
          buf.p = tmp[f*100+i*10+j].p;
	  printf("[%d %d] %d ->%d\n", i, j, f, buf.p);
	}
      }
    printf("\n");
    }
  printf("\n");
  }
  return buf;
}

int points_total(PLAN grand){
  return point_V(grand) + point_R(grand) + point_L(grand) + point_F(grand);
}

int points_total2(PLAN grand){
  return point_V2(grand) + point_R2(grand) + point_L2(grand) + point_F2(grand) + point_P2(grand);
}

/*solveur le l'algorithme proposé */
int Honshu_opti(tuile T[], PLAN grand, int fois, pile* tmp_case, int car_usee[]){
  int i = 0, j = 0, f = 0, t = 0;
  int num = 0;
  int Best;
  int score;
  bool buf = false;
  for (i = 0 ; i  < 12; i++){
    if (car_usee[i] == 0)
      buf = true;
    else{
      num++;
    }
  }
  if (!buf){
    affiche(grand.plan);
    return point_V(grand) + point_L(grand) + point_F(grand) + point_R(grand);
  }
  else{
    i = 0;
    Best = -1;

    for(t = 0; t < 12; t++){
      if(car_usee[t] == 0){
	tuile TMP = T[t]; 
	for(f = 0; f < 4; f++){
	  TMP = rotation_n(TMP, f);
	  
	  for(i = 0; i < 10; i++){
	    for(j = 0; j < 10; j++){
	      bool flag = insertion_validation(TMP, grand, i, j , fois, tmp_case, car_usee);
	      printf("\nFOIS ICI %d\n",fois);
	      if (flag){
		/*fois++;*/
		car_usee[t] = 1;
		score = Honshu_opti(T, grand, fois++, tmp_case, car_usee);
		
		printf("!!!!!!!Le score ici est : %d\n", score);
		if (score > Best)
		  Best = score;
		cheki(TMP, grand,i,j,tmp_case,car_usee);
	      }
	    }
	  }
	}
      }
    }
    /*affiche(grand.plan);*/
    return Best;
  }
}
