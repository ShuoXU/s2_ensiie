#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/* Il est nécéssaire d'importer struct.h pour utiliser draw.h */


/*
* \brief creer une tuile aléatoire et lui attribue l'id ID
* \param un ID de type int
* \return la tuile créée
*/
tuile creer_tuile_aleatoire(int ID);
/*
* \brief recupere la n-ieme tuile dans le fichier data.txt
* \param n de type int
* \return la tuile recuperée
*/
tuile creer_tuile_fixe(int n );

/*
* \brief fonction de tirage de la nieme carte du fichier et lui attribue l'id ID
* \param stack de type int[], n et ID de type int
* \return la tuile tirée
*/
tuile tirer_carte_fixe(int stack[],int n , int ID);


/*
* \brief fonction de tirage d'une random carte du fichier et lui attribue l'id ID
* \param stack de type int[], ID de type int
* \return la tuile tirée
*/
tuile tirer_carte_aleatoire(int stack[], int ID);

