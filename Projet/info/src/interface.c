#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <string.h>
#include "struct.h"
#include "alloc.h"
#include "draw.h"
#include "stack.h"
#include "affichage.h"
#include "rotation.h"
#include "insertion.h"
#include "points.h"


/*Selection du mode de jeu */
int gamemode()
{
  int a=0;
  printf("Veuillez selectionner le mode de jeu desiré : \n1. Aléatoire \n2. Avec choix \n3. Charger une partie\n");
  scanf("\n%d", &a);
  system("clear");
  while(a!=2 && a!=1 && a!=3){
    printf("Veuillez selectionner le mode de jeu desiré : \n1. Aléatoire \n2. Avec choix \n3. Charger une partie\n");
    scanf("%*s");
    scanf("\n%d", &a);
    system("clear");
  }
  return a;
}

int reglemode()
{
  int a=0;
  printf("Veuillez selectionner la regle qui vous interesse : \n1. Regle initiale \n2. Regle complexe\n");
  scanf("\n%d", &a);
  system("clear");
  while(a!=2 && a!=1){
    printf("Veuillez selectionner la regle qui vous interesse : \n1. Regle initiale \n2. Regle complexe\n");
    scanf("%*s");
    scanf("\n%d", &a);
    system("clear");
  }
  printf("\nVous avez choisi la regle No.%d\n", a);
  return a;
}

/* Tirage des 12 cartes */
void draw(tuile *main){
  int i;
  int stack[21];
  int o;
  for(o=0;o<21;o++){
    stack[o]=0;
  }
  for (i=0;i<12;i++){
    main[i]=tirer_carte_aleatoire(stack,i);
  }
  
}

/*interface de rotation d'une tuile */
tuile rotationInt(tuile tuile){
  int a;
  printf("Combien de fois ? (sens anti horaire)\n");
  scanf("%d", &a);
  tuile=rotation_n(tuile,a);
  return(tuile);	
}


int main (){
  /* Mise en place des variables utiles à l'interface */
  /****************************************************/
  system("clear");
  int gm=gamemode();
  int rm=reglemode();
  printf("Ici regle :%d\n", rm);
  PLAN map;
  
  tuile mains[12];	
  int car_usee[12];
  tuile tuile;
  int o;
  for(o=0;o<12;o++){
    car_usee[o]=0;
  }
  pile *ps=malloc(sizeof(pile));;
  stack_init(ps);
  int tour=0;
  int i,a;
  int min=0;
  
  /***************************************************/
  if (gm!=3){
    draw(mains);
    map.plan=allocation(10,10);
    map.flag=allocation_of_int(10,10);
  }
  else{
    /*ajouter le mode "charger fichier"*/
    int wj,xj;
    xj=0;
    tour=1;
    tab infosp;
    char fname[100];
    char path[]="ressources/";
    printf("Entrez le nom du fichier à charger( Partie1.txt / Partie2.txt) :");
    scanf("%s",fname);
    strcat(path,fname);
    while(!exists(path)){
      printf("\nEntrez un nom de fichier valide :");
      scanf(" %s",fname);
      strcat(path,fname);
    }
    extract(infosp,path);
    int stack[21];
    map.plan=allocation(infosp[0],infosp[1]);
    map.flag=allocation_of_int(infosp[0],infosp[1]);
    insertion_validation(tirer_carte_fixe(stack,infosp[14],0),map,0,0,0,ps,car_usee);
    for(wj=2;wj<14;wj++){
      mains[xj]=tirer_carte_fixe(stack,infosp[wj],xj);
      xj++;
    }
  }

  
  printf("\n \n \n \n");
  system("clear");
  while(min<12 && plan_tout_rempli(map)){ /* condition de fin de partie */
    affiche(map.plan);
    if (rm==1){
      printf("\nRegle 1\n______________________\n");
      int pv=point_V(map),pf=point_F(map),pl=point_L(map),pr=point_R(map);
      printf("points V : %d\n",pv);
      printf("points F : %d\n",pf);
      printf("points L : %d\n",pl);
      printf("points R : %d\n",pr);
      printf("score : %d\n",/*pv+*/pl+pr+pf);
      printf("_______________________________\n");
    }
    else{
      printf("\nRegle 2\n______________________\n");
      int pv=point_V2(map),pf=point_F2(map),pl=point_L2(map),pr=point_R2(map),pp=point_P2(map);
      printf("points V : %d\n",pv);
      printf("points F : %d\n",pf);
      printf("points L : %d\n",pl);
      printf("points R : %d\n",pr);
      printf("points P : %d\n",pp);
      printf("score : %d\n",pv+pl+pr+pf+pp);
      printf("_______________________________\n");
    }
    /* Affichage de l'interface pour chaque tour */
    /*********************************************/
    min =12;
    affiche_orizontale(mains,car_usee);
    for(i=11;i>=0;i--){
      if (car_usee[mains[i].id]==0){
	if (i<min){
	  min=i;
	}
	/*
	  printf("Tuile n° %d\n",i+1);
	  affiche(mains[i].casse);
	  printf("\n -------\n");*/
      }
    }
    /***********************************************/
    /* Selection de la tuile si le joueur n'est pas dans le mode de jeu aléatoire */
    /******************************************************************************/
    if (gm==1 || gm==3){
      tuile=mains[min];
    }
    if (gm==2){
      printf("Renseignez le numero de la tuile que vous voulez prendre :\n");
      scanf("%d",&a);
      /*char tmp;
      scanf("%c", &tmp);
      if(tmp == 'Q' || tmp == 'q' )
	goto here;
	a = tmp - 48;*/
      tuile=mains[a];
    }
    /********************************************************************************/
    char str='x';
    int x,y;
    /*Choix de l'action du joueur*/
    /*********************************************************************************/
    while ((str!='A')&&(str!='a')){
      system("clear");
      affiche(map.plan);
      if (rm==1){
	printf("\nRegle 1\n___________________\n");
	int pv=point_V(map),pf=point_F(map),pl=point_L(map),pr=point_R(map);
	printf("points V : %d\n",pv);
	printf("points F : %d\n",pf);
	printf("points L : %d\n",pl);
	printf("points R : %d\n",pr);
	printf("score : %d\n",pv+pl+pr+pf);
	printf("____________________________\n");
      }
      else{
	printf("\nRegle 2\n___________________\n");
	int pv=point_V2(map),pf=point_F2(map),pl=point_L2(map),pr=point_R2(map),pp=point_P2(map);
	printf("points V : %d\n",pv);
	printf("points F : %d\n",pf);
	printf("points L : %d\n",pl);
	printf("points R : %d\n",pr);
	printf("points P : %d\n",pp);
	printf("score : %d\n",pv+pl+pr+pf+pp);
	printf("____________________________\n");
      }
      affiche(tuile.casse);
      printf("Pour rotate : entrez R\nPour ajouter la tuile : entrez A\nPour connaitre la meilleure solution : entrez S\nPour quitter : entrez Q\n");
      scanf("\n%c", &str);

      if(str == 'Q' || str == 'q'){
	goto here;
      }

      
      if ((str=='R')||(str=='r')){
	tuile=rotationInt(tuile);
	system("clear");
	affiche(map.plan);
	if (rm==1){
	  printf("\nRegle 1\n______________________\n");
	  int pv=point_V(map),pf=point_F(map),pl=point_L(map),pr=point_R(map);
	  printf("points V : %d\n",pv);
	  printf("points F : %d\n",pf);
	  printf("points L : %d\n",pl);
	  printf("points R : %d\n",pr);
	  printf("score : %d\n",pv+pl+pr+pf);
	  printf("_______________________________\n");
	}
	else{
	  printf("\nRegle 2\n______________________\n");
	  int pv=point_V2(map),pf=point_F2(map),pl=point_L2(map),pr=point_R2(map),pp=point_P2(map);
	  printf("points V : %d\n",pv);
	  printf("points F : %d\n",pf);
	  printf("points L : %d\n",pl);
	  printf("points R : %d\n",pr);
	  printf("points P : %d\n",pp);
	  printf("score : %d\n",pv+pl+pr+pf+pp);
	  printf("_______________________________\n");
	}
	affiche(tuile.casse);
      }
      
      if ((str=='A')||(str=='a')){
	if(tour!=0){
	  printf("Entrez les coordonnées voulues pour le point en haut à gauche\n");
	  printf("x=?\n");
	  scanf("%d",&x);
	  printf("y=?\n");
	  scanf("%d",&y);
	  insertion_validation(tuile,map,x,y,tour,ps,car_usee);/* insertion de la tuile */
	}
	else{
	  x=0;
	  y=0;
	  insertion_validation(tuile,map,x,y,tour,ps,car_usee);/* insertion de la tuile */
	  
	  /************************************************************************************/
	  sleep(2);
	  system("clear");
	}
      }
      
      if ((str=='S')||(str=='s')){
	points sb = sol_bete (tuile, map,tour, ps, car_usee, rm);
        
	system("clear");
	printf("Le point le plus optimal est: [%d %d]->%d, facon rotation : %d\n", sb.x, sb.y, sb.p, sb.r);
	printf("Voulez vous continuer ? Si oui entrez Y sinon N\n");
	scanf("\n%c", &str);
        
	if (str=='Y'||str=='y'){
	  str='A';
	  tuile = rotation_n(tuile, sb.r);
          affiche(tuile.casse);
          printf("Le point ici : %d %d",sb.x,sb.y);
	  insertion_validation(tuile,map,sb.x,sb.y,tour,ps,car_usee);
	  sleep(1);
	}
      }
      
      if (str=='H'||str=='h'){
	int score = Honshu_opti(mains, map,tour, ps, car_usee);		
	printf("Le score maximal de ces cartes est: %d\n", score);
	scanf("\n%c", &str);
	if (str=='Y'||str=='y'){
	  str='A';
	}
	printf("Merci de jouer!\n");
	break;
	

      }
    }
    
    /* reset de l'interface */
    sleep(1);
    system("clear");
    tour++;
    min =12;
    for(i=11;i>=0;i--){
      if (car_usee[mains[i].id]==0){
	if (i<min){
	  min=i;
	}
      }
    }

  }
 here:;
  system("clear");
  printf("Fin de jeu!!\n");
  affiche(map.plan);
  printf("Votre score finale : %d\n",points_total(map));
  printf("Bien joué!!!!!!!!");
  return 1;
}
