#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


/* fonction affichant les éléments d'un stack */
void print_stack(pile *ps);

/* fonction pop pour stack */
couple pop(pile* ps);

/* fonction push pour stack */
void push(pile* ps, couple tmp);

/* test pour savoir si ps est vide */
int est_vide(pile* ps);

/* initialisation du stack ps */
void  stack_init(pile *ps);