#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


/*
* \brief Affiche un array
* permet d'afficher les données du couple
* \param arr de type couple[],size de type int
*/

void array_print(couple arr[], int size);

/*
* \brief Affiche une Matrix
* permet d'afficher les données d'un plateau ou d'une tuile 
* \param ret de type matrix 
*/
void affiche(matrix ret);

/*
* \brief Affiche une Matrix_of_int
* permet d'afficher les données de la matrice int des tuiles
* \param ret de type matrix_of_int 
*/
void affiche_of_int(matrix_of_int ret);

/*
* \brief Affiche les tuiles d'une liste
* permet d'afficher les tuiles en attente
* \param stack de type tuile[]
*/

void affiche_lot_carte (tuile stack[]);


void affiche_orizontale (tuile T[], int car_usee[]);

