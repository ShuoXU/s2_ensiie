#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/* Test afin de determiner si la tuile en train d'être placée ne depasse pas du plateau */
bool dehors_borne (tuile petit, PLAN grand,int x, int y);

/* Test de recouvrement de lac */
bool lac_decouvert (tuile petit, PLAN grand, int x, int y);


/*fonction permettant de retirer une tuile du plan */
void cheki(tuile petit, PLAN grand, int x, int y, pile * tmp_case, int car_usee[]);

/* Test afin de determiner si une tuile est totalement recouverte */
bool carte_total_decouvert (PLAN grand, int car_usee[]);


/*insertion sans test de la tuile sur le plateau */
void insertion (tuile petit, PLAN grand, int x, int y, pile* tmp_case, int car_usee[]);


/*insertion avec test de la tuile sur le plateau */
bool insertion_validation(tuile petit, PLAN grand, int x, int y, int fois, pile* tmp_case, int car_usee[]);

/* test si le plateau est plein pour determiner la fin de partie */
bool plan_tout_rempli(PLAN grand);

points sol_bete (tuile petit, PLAN grand, int fois, pile* tmp_case, int car_usee[], int rm);


int Honshu_opti(tuile T[], PLAN grand, int fois, pile* tmp_case, int car_usee[]);
