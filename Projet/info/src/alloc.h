#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/* Il est nécéssaire d'importer struct.h pour utiliser alloc.h */

/*
* \brief Allocation dynamique d’une grille de jeu carrée de taille variable
* toutes les cases de notre matrice sont initialisé avec le string ‘E’ (pour Empty)
* \attention cette fonction sera utililer dans la fonction de création d'une tuile
* \param  n :lenght pour définir la longueur de la matrice  et m: width pour sa largeur
* \return une matrice de taille (n,m)
*/

matrix allocation (int n , int m );


/* Même principe que allocation mais pour les matrix_of_int */
matrix_of_int allocation_of_int(int n , int m );



/*
* \brief Libère la mémoire dynamique allouée à x
* \param  x de type matrix
*/
void free_matrice(matrix x);



/* Même principe que free_matrice mais pour les matrix_of_int */
void free_matrice_of_int(matrix_of_int x);

void extract(tab v, char *nomFichier);

int exists(char *nomFichier);