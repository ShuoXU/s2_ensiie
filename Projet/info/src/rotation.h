#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>




/**
* \brief Rotation d’une tuile 
*  faire une seule rotation de 90° dans le sens direct(sens antihoraire) 
* \param m de type matrix (une tuile)
* \return une tuile de type matrix 
*/
tuile rotation (tuile m);


/**
* \brief Etablir le nombre de fois que le joueur souhaite tourner la tuile
* \param m: la matrice à inverser, n: nombre de fois que le joueur souhaite tourner la tuile
* \return une matrice de type matrix inversée
*/
tuile rotation_n (tuile  m, int n);
