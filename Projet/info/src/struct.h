#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 100

typedef int tab[15];


typedef struct matrice
{
  char ** cells;
  int length;
  int width;
}matrix;

typedef struct matrice_of_int
{
  int ** cells;
  int length;
  int width;
}matrix_of_int;

typedef struct PLAN{
  matrix plan;
  matrix_of_int flag;
}PLAN;

typedef struct tuile
{
  matrix casse;
  int id;
  int coordonnee[2];
}tuile;

typedef struct couple{
  int elemInt;
  char elemChar;
}couple;
  
typedef struct
{
  int  count;
  couple elemCouple[MAX_SIZE];
}pile;

typedef struct points{
  int x;
  int y;
  int p;
  int r;
}points;
